# stage 1 - golang
FROM golang:1.18-alpine3.16 as build

WORKDIR /app
COPY ./todo-project-backend . 
RUN go build -o api

#stage 2
FROM node:18-alpine3.15 as node
WORKDIR /app
COPY ./todo-project-frontend .
RUN npm install
RUN npm run build

# # stage 3 - combine
# FROM nginx:alpine
# RUN apk --no-cache add ca-certificates
# COPY --from=node /app/dist/todo-project /usr/share/nginx/html
# COPY --from=build /app/server ./
# EXPOSE 80

FROM alpine
RUN apk --no-cache add ca-certificates
WORKDIR /app/server/
COPY --from=build /app/api /app/server/
COPY --from=node /app/dist/ /app/ui/dist
EXPOSE 80
CMD ["./api"]
