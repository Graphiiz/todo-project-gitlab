import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  todos = ['eat','sleep','play'];
  name !: string;

  // change according to server ip
  backEndURL = 'http://172.20.10.8:1234/'

  constructor(private http: HttpClient) { }

  getLoginResponseFromEmail(email: string){
    // get name via http -> redirect to todos/:name
    console.log(this.backEndURL+'login/'+email);

    return this.http.get<User>(this.backEndURL+'login/'+email);

  }

  createNewUser(email: string){
    // create User object from email with empty array of todos
    const newUser: User = {
      id: "",
      email: email,
      todos: []
    }
    this.http.post<User>(this.backEndURL+'login/'+email, newUser).subscribe((response)=>{
      console.log(response)
    });
  }

  getTodoResponseFromId(id: string){

    return this.http.get<User>(this.backEndURL+'todos/'+id);

  }

  UpdateAndSaveToDB(userData: User){
    const id = userData["id"]
    this.http.post<User>(this.backEndURL+'todos/'+id, userData)
    .subscribe((response)=>{
      console.log(response)
      alert('Save Successfully.')
    }, error => {
      console.log(error.message)
    });

  }

  deleteTodo(){
    // delete a single todo from todoList
    // In this project,we delete the data directly at the database. This function is not required.
  }

}
