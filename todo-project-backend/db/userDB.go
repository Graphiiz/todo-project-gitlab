package db

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type UserDB struct {
	gorm.Model
	// Code  string
	// Name  string
	Id    string `gorm:"primaryKey"`
	Email string
	Todos []byte
}

func ReadEmail(email string) (*UserDB, error) {
	db := DB()

	var userDB UserDB
	if err := db.First(&userDB, "Email = ?", email).Error; err != nil {
		return nil, err
	}

	return &userDB, nil

}

func ReadId(id string) (*UserDB, error) {
	db := DB()

	var userDB UserDB
	if err := db.First(&userDB, "Id = ?", id).Error; err != nil {
		return nil, err
	}

	return &userDB, nil

}

func Create(user UserDB) error {
	db := DB()
	userData := UserDB{
		Id:    user.Id,
		Email: user.Email,
		Todos: user.Todos,
	}

	if err := db.Create(&userData).Error; err != nil {
		return err
	}

	return nil
}

func Save(user UserDB) error {
	db := DB()
	var userDB UserDB
	if err := db.First(&userDB, "Id = ?", user.Id).Error; err != nil {
		return err
	}

	userDB.Todos = user.Todos

	if err := db.Save(&userDB).Error; err != nil {
		return err
	}

	return nil
}

func (user *UserDB) BeforeCreate(tx *gorm.DB) (err error) {
	// UUID version 4
	user.Id = uuid.NewString()
	return
}
