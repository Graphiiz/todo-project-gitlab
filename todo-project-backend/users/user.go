package users

type User struct {
	Id    string   `json:"id"`
	Email string   `json:"email"`
	Todos []string `json:"todos"`
}
